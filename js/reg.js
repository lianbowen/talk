const loginIdValidator = new FieldValidator("txtLoginId", function (val) {
  if (!val) {
    return "请输入账号";
  }
});
const nicknameValidator = new FieldValidator("txtNickname", function (val) {
  if (!val) {
    return "请输入昵称";
  }
});
const loginPwdValidator = new FieldValidator("txtLoginPwd", function (val) {
  if (!val) {
    return "请输入密码";
  }
});
const loginPwdConfirmValidator = new FieldValidator(
  "txtLoginPwdConfirm",
  function (val) {
    if (!val) {
      return "请输入确认密码";
    }
    if (val !== loginPwdValidator.input.value) {
      return "请重新输入确认密码";
    }
  }
);

const form = document.querySelector(".user-form");

form.onsubmit = async function (e) {
  e.preventDefault();
  const result = await FieldValidator.validate(
    loginIdValidator,
    nicknameValidator,
    loginPwdValidator,
    loginPwdConfirmValidator
  );
  if (!result) {
    return; //"验证未通过";
  }
  const formData = new FormData(form);
  const data = Object.fromEntries(formData.entries());
  const resp = await API.reg(data);
  if (resp.code === 0) {
    alert("注册成功即将跳转到登录");
    location.href = "./login.html";
  } else {
    alert("注册失败，请检查是否填写有误");
  }
};
