const loginIdValidator = new FieldValidator("txtLoginId", function (val) {
  if (!val) {
    return "请输入账号";
  }
});

const loginPwdValidator = new FieldValidator("txtLoginPwd", function (val) {
  if (!val) {
    return "请输入密码";
  }
});

const form = document.querySelector(".user-form");

form.onsubmit = async function (e) {
  e.preventDefault();
  const result = await FieldValidator.validate(
    loginIdValidator,
    loginPwdValidator
  );
  if (!result) {
    return; //"验证未通过";
  }
  const formData = new FormData(form);
  const data = Object.fromEntries(formData.entries());
  const resp = await API.login(data);
  if (resp.code === 0) {
    location.href = "./index.html";
  } else {
    alert("登陆失败，请检查账号或密码是否有误");
  }
};
