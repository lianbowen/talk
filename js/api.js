const API = (() => {
    const BASE_URL = "https://study.duyiedu.com";
    const TOKEN_KEY = "token";

    async function get(path) {
        const headers = {};
        const token = localStorage.getItem(TOKEN_KEY);
        if (token) {
            headers.authorization = `Bearer ${token}`;
        }
        return fetch(BASE_URL + path, { headers });
    }

    async function post(path, bodyObj) {
        const headers = {
            "Content-Type": "application/json",
        };
        const token = localStorage.getItem(TOKEN_KEY);
        if (token) {
            headers.authorization = `Bearer ${token}`;
        }
        return fetch(BASE_URL + path, {
            headers,
            method: "POST",
            body: JSON.stringify(bodyObj),
        });
    }

    async function reg(regInfo) {
        const resp = await post("/api/user/reg", regInfo);
        return await resp.json();
    }

    async function login(loginInfo) {
        const resp = await post("/api/user/login", loginInfo);
        const result = await resp.json();
        if (result.code === 0) {
            //将响应头的token 保存到 localStorage
            const token = resp.headers.get("authorization");
            localStorage.setItem(TOKEN_KEY, token);
        }
        return result;
    }

    async function exists(loginId) {
        const resp = await get("/api/user/exists?loginId=" + loginId);
        return await resp.json();
    }

    async function getProfile() {
        const res = await get("/api/user/profile");
        return await res.json();
    }

    async function sendMessage(content) {
        const resp = await post("/api/chat", {
            content,
        });
        return await resp.json();
    }

    async function getHistory() {
        const resp = await get("/api/chat/history");
        return await resp.json();
    }

    function loginOut() {
        localStorage.removeItem(TOKEN_KEY);
    }
    return {
        reg,
        loginOut,
        login,
        exists,
        getProfile,
        sendMessage,
        getHistory,
    };
})();
