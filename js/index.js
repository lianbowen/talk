(async function () {
  //获取已登录用户信息
  const profile = await API.getProfile();
  const data = profile.data;
  //如果data为null 说明没有用户信息 跳转到登录页进行登录
  if (!data) {
    alert("当前账号未登录或已过期,请重新登录");
    location.href = "./login.html";
    return;
  }
  const doms = {
    aside: {
      loginId: document.querySelector("#loginId"),
      nickname: document.querySelector("#nickname"),
    },
    close: document.querySelector(".close"),
    chatContainer: document.querySelector(".chat-container"),
    txtMsg: document.querySelector("#txtMsg"),
    msgContainer: document.querySelector(".msg-container"),
  };

  //以下代码为 获取到用户信息后执行的操作
  setInfo();
  //将获取到的用户id和用户昵称 渲染到页面
  function setInfo() {
    doms.aside.loginId.innerText = data.loginId;
    doms.aside.nickname.innerText = data.nickname;
  }

  //点击关闭按钮后 进行注销并返回登录页面
  doms.close.onclick = function () {
    API.loginOut();
    location.href = "./login.html";
  };
  //form 回车或者点击发送按钮后进行发送聊天到服务器
  doms.msgContainer.onsubmit = function (e) {
    e.preventDefault();
    console.log("提交了");
    sendChat();
  };

  //获取聊天记录
  await getHistory();
  async function getHistory() {
    const resp = await API.getHistory();
    for (const item of resp.data) {
      addChat(item);
    }
    console.log(resp.data);
    scrollBottom();
  }

  //添加聊天内容
  async function addChat(chatInfo) {
    let from = chatInfo.from;

    const div = document.createElement("div");
    div.classList.add("chat-item");
    if (from) {
      div.classList.add("me");
    }

    const img = document.createElement("img");
    img.className = "chat-avatar";
    img.src = `${from ? "./asset/avatar.png" : "./asset/robot-avatar.jpg"}`;

    const content = document.createElement("div");
    content.className = "chat-content";
    content.innerText = chatInfo.content;

    const date = document.createElement("div");
    date.classList.add("chat-date");
    date.innerText = formatDate(chatInfo.createdAt);

    div.appendChild(img);
    div.appendChild(content);
    div.appendChild(date);

    doms.chatContainer.appendChild(div);
  }

  // 让聊天区域的滚动条滚动到底
  function scrollBottom() {
    doms.chatContainer.scrollTop = doms.chatContainer.scrollHeight;
  }

  //格式化时间
  function formatDate(timestamp) {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hour = date.getHours().toString().padStart(2, "0");
    const minute = date.getMinutes().toString().padStart(2, "0");
    const second = date.getSeconds().toString().padStart(2, "0");
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  }

  //发送聊天
  async function sendChat() {
    const content = doms.txtMsg.value;
    if (!content) {
      return;
    }
    addChat({
      from: data.loginId,
      to: null,
      createdAt: Date.now(),
      content,
    });
    // 将滚动条设置到最后一条的位置
    scrollBottom();
    // 清空输入框的内容
    doms.txtMsg.value = "";

    //等待拿到发送的返回数据后
    const resp = await API.sendMessage(content);
    //添加聊天到页面
    addChat({
      from: null,
      to: data.loginId,
      ...resp.data,
    });
    //在滚动到最后一条聊天的位置
    scrollBottom();
  }
})();
